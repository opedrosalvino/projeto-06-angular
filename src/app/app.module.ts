import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HelloComponent } from './hello.component';
import { RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { DisciplinasComponent } from './disciplinas/disciplinas.component';
import { TemporizadorComponent } from './temporizador/temporizador.component';
import { SubjectsService } from './subjects.service';
import { TimerService } from './timer.service';
import { NavbarComponent } from './navbar/navbar.component';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent },
      { path: 'temporizador', component: TemporizadorComponent },
      { path: 'disciplinas', component: DisciplinasComponent }
    ])
  ],
  declarations: [
    AppComponent,
    HelloComponent,
    NavbarComponent,
    HomeComponent,
    DisciplinasComponent,
    TemporizadorComponent
  ],
  bootstrap: [AppComponent],
  providers: [SubjectsService, TimerService]
})
export class AppModule {}
