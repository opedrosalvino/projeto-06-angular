import { Injectable } from '@angular/core';

interface Subject {
  name: string;
}

@Injectable()
export class SubjectsService {
  list: Array<Subject> = [];
  constructor() {
    this.adicionarDisciplina('Desenvolvimento para Dispositivos Móveis I');
    this.adicionarDisciplina('Desenvolvimento para Servidores II');
    this.adicionarDisciplina('Tópicos Especiais em Sistemas para Internet II');
    this.adicionarDisciplina('Negócios e Marketing Eletrônicos');
    this.adicionarDisciplina(
      'Projeto de Graduação em Sistemas para Internet I'
    );
    this.adicionarDisciplina(
      'Estágio Supervisionado em Sistemas para Internet I'
    );
    this.adicionarDisciplina('Projeto de Prototipagem e Testes de Usabilidade');
  }

  getDisciplinas() {
    return this.list;
  }

  adicionarDisciplina(name: string) {
    if (name != '') {
      this.list.push({ name });
    }
  }

  removerDisciplina(index: number) {
    this.list.splice(index, 1);
  }
}
