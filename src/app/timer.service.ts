import { Injectable } from '@angular/core';

@Injectable()
export class TimerService {
  private timer: any;
  private counter = 0;

  constructor() {}

  iniciarContagem(intervalo: string) {
    if (!this.timer && intervalo != '' && intervalo != '0') {
      let interval = parseInt(intervalo);
      this.timer = setInterval(() => {
        this.counter++;
      }, interval);
    }
  }

  pararContagem() {
    if (this.timer) {
      clearInterval(this.timer);
      this.timer = null;
    }
  }

  zerarContagem() {
    this.pararContagem();
    this.counter = 0;
  }

  getContagem() {
    return this.counter;
  }
}
